import Vue from 'vue'
import VueRouter from 'vue-router'
import { Button } from 'vant'
import Article from '@/views/ArticleJky'
import Home from '@/views/HomeJky'
import Login from '@/views/LoginJky'
import Overview from '@/views/OverviewJky'
import Publish from '@/views/PublishJky'
Vue.use(Button)
Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    { path: '/login', component: Login },
    {
      path: '/',
      component: Home,
      redirect: '/home',
      children: [
        { path: '/home', component: Home },
        { path: '/article', component: Article },
        { path: '/overview', component: Overview },
        { path: '/publish', component: Publish }

      ]
    }
  ]

})

export default router
